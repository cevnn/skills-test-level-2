var express = require('express');
var path = require('path');
var cors = require('cors');

var whitelist = ['http://localhost:4200']
var corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(null, true)
        }
    }
}

var app = express();

app.use(express.json());

app.use(cors(corsOptions))

app.get('/', function (req, res) {
  res.send('Hello World!')
})

module.exports = app;
